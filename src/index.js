const { app, BrowserWindow, Menu, ipcMain } = require("electron");

const path = require("path");

if (process.env.NODE_ENV !== "production")
  require("electron-reload")(__dirname, {});

let mainWindow;
let newProductWindow;
app.on("ready", () => {
  mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
    },
  });

  mainWindow.loadFile(path.join(__dirname, "views/index.html"));

  const mainMenu = Menu.buildFromTemplate(templateMenu);
  Menu.setApplicationMenu(mainMenu);

  mainWindow.on("closed", () => {
    app.quit();
  });
});

function createNewProductWindow() {
  newProductWindow = new BrowserWindow({
    width: 400,
    height: 330,
    title: "Add product",
    webPreferences: {
      nodeIntegration: true,
    },
  });

  //newProductWindow.setMenu(null);
  newProductWindow.loadFile(path.join(__dirname, "views/create-product.html"));

  newProductWindow.on("closed", () => {
    newProductWindow = null;
  });
}

ipcMain.on('newProduct',(e,newProduct)=>{
  mainWindow.webContents.send('newProduct',newProduct);
  newProductWindow.close();
});


const templateMenu = [
  {
    label: "File",
    submenu: [
      {
        label: "Add product",
        accelerator: process.platform == "darwin" ? "command+N" : "Ctrl+N",
        click() {
          console.log("new product");
          createNewProductWindow();
        },
      },
      {
        label: "Remove all products",
        click(){
          mainWindow.webContents.send('removeAllProducts');
        }
      },
      {
        label: "Close application",
        accelerator: process.platform == "darwin" ? "command+Q" : "Ctrl+Q",
        click(){
          app.quit();
        }
      },
    ],
  }
  
];

if(process.platform =="darwin")
{
  templateMenu.unshift({
    label:app.getName()
  });
}


if(process.env.NODE_ENV !== 'production'){
  templateMenu.push(
    {
      label: 'Debug',
      submenu:[
        {
          label: 'toogle dev tools',
          accelerator:'Ctrl+D',
          click(item,focusedWindow){
            focusedWindow.toggleDevTools();
          }
        },
        {
          role: 'reload'
        }
      ]
    }
  );
}